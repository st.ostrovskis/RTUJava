package nodarbiba7;

public enum REASON {
    RESULT("Saņemt analižu rezultatus"), ANALYZES("Nodot annalizes"), VACCINATION("Vakcinacija");

    private final String name;

    REASON(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }
}
