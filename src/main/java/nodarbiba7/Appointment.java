package nodarbiba7;


import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.InputMismatchException;
import java.util.Scanner;

import static nodarbiba7.REASON.*;

public class Appointment implements Serializable {
    private REASON reason;
    private LocalDateTime appointmentDate = LocalDateTime.now();

    Appointment() {
        createAppointment();
        System.out.println("Pakalpojuma veids: " + reason.getName());
        System.out.print(appointmentDate.getDayOfMonth() + " " + appointmentDate.getMonth() + " ");
        System.out.println(appointmentDate.format(DateTimeFormatter.ofPattern("yyyy HH:mm")));
    }

    // Izveido vizites iemeslu un datumu ar laiku
    private void createAppointment() {
        createReason();
        createDate();
        createTime();
    }

    //Izveido vizites iemeslu
    private void createReason() {
        StringBuilder info = new StringBuilder("Ievadiet pakalpojuma veida numuru: \n");
        Scanner scanner = new Scanner(System.in);
        System.out.println(info.
                append("1(").append(ANALYZES.getName()).append(") : ").
                append("2(").append(RESULT.getName()).append(") :").
                append("3(").append(VACCINATION.getName()).append(") "));
        try {
            int choice = scanner.nextInt();
            reason = switch (choice) {
                case 1 -> ANALYZES;
                case 2 -> RESULT;
                case 3 -> VACCINATION;
                default -> null;
            };

        } catch (InputMismatchException e) {
            System.out.println("Jus ievadijat nepareizu numuru!");
            createReason();
        }
        if (reason == null) {
            System.out.println("Jus ievadijat nepareizu numuru!");
            createReason();
        }
    }

    //Izveido datumu
    private void createDate() {
        System.out.println("Šodienas datums : " + LocalDateTime.now().format(DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT)));
        chooseMonth();
        System.out.println("Jus izvelejaties: " + appointmentDate.getMonth());
        appointmentDate = appointmentDate.withDayOfMonth(chooseDay());

        //Ja izveidotais datums ir pirms ritdienas 8:00, izveido ciklu ar iespeju no jauna izveidot datumu
        while (appointmentDate.isBefore(LocalDateTime.now().plusDays(1).withHour(8).withMinute(0))) {
            System.out.println("Nepareiz datums.");
            createDate();
        }
    }

    //Ļauj izveleties menesi
    private void chooseMonth() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Gribat pierakstities uz " +
                "1(" + LocalDate.now().getMonth() + ") : 2(" + LocalDate.now().plusMonths(1).getMonth() + ")");
        try {
            int choice = scanner.nextInt();
            int month;
            if (choice == 2) {
                month = LocalDate.now().plusMonths(1).getMonthValue(); // pierakstities uz nakošo menesi
            } else {
                month = LocalDate.now().getMonthValue(); // pierakstities uz šo menesi
            }
            appointmentDate = appointmentDate.withMonth(month);
        } catch (InputMismatchException e) {
            System.out.println("Jus ievadijat nepareizu numuru!");
            chooseMonth();
        }
    }

    //Ļauj izvelieteis dienu
    private int chooseDay() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ievadiet dienu:");
        int day;
        try {
            day = scanner.nextInt();
            //Ja ievaditais skaitlis mazak par 1 vai ari lielaks par menesi maksimalu dienu skaitu
            //izveido ciklu ar iespeju no jauna ievadit dienu
            while (day <= 0 || day > LocalDate.MAX.withMonth(appointmentDate.getMonthValue()).lengthOfMonth()) {
                System.out.println("Tadas dienas nav!");
                day = chooseDay();
            }

        } catch (InputMismatchException e) {
            System.out.println("Jus ievadijat nepareizu dienu!");
            day = chooseDay();
        }

        return day;
    }

    //Izveido laiku
    private void createTime() {
        System.out.println("Jus izvelejaties: " + appointmentDate.getDayOfMonth() + " " + appointmentDate.getMonth());
        int hours = chooseHours();
        int minutes = chooseMinutes();
        appointmentDate = appointmentDate.withHour(hours).withMinute(minutes);
    }

    //Ļauj izvelieties stundu
   private int chooseHours() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Izvelieties laiku: " +
                "8 : 9 : 10 : 11 : 12 : 13 : 14 : 15 : 16 : 17 : 18");
        int hours;
        try {
            int choice = scanner.nextInt();
            hours = switch (choice) {
                case 8 -> HOURS.EIGHT.getHours();
                case 9 -> HOURS.NINE.getHours();
                case 10 -> HOURS.TEN.getHours();
                case 11 -> HOURS.ELEVEN.getHours();
                case 12 -> HOURS.TWELVE.getHours();
                case 13 -> HOURS.THIRTEEN.getHours();
                case 14 -> HOURS.FOURTEEN.getHours();
                case 15 -> HOURS.FIFTEEN.getHours();
                case 16 -> HOURS.SIXTEEN.getHours();
                case 17 -> HOURS.SEVENTEEN.getHours();
                case 18 -> HOURS.EIGHTEEN.getHours();
                default -> 0;
            };

            //Ja ievaditais skaitli  neatbilst iespejamiem variantiem, izveido ciklu ar iespeju ievadit no jauna
            while (hours == 0) {
                hours = chooseHours();
            }
        } catch (InputMismatchException e) {
            System.out.println("Jus ievadijat nepareizu laiku. Meģiniet velreiz!");
            hours = chooseHours();
        }
        return hours;
    }

    //Ļauj izvelietes minutes
    private int chooseMinutes() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("00 : 10 : 20 : 30 : 40 : 50");
        int minutes;
        try {
            int choice = scanner.nextInt();
            minutes = switch (choice) {
                case 0 -> MINUTES.ZERO.getMinutes();
                case 10 -> MINUTES.TEN.getMinutes();
                case 20 -> MINUTES.TWENTY.getMinutes();
                case 30 -> MINUTES.THIRTY.getMinutes();
                case 40 -> MINUTES.FORTY.getMinutes();
                case 50 -> MINUTES.FIFTY.getMinutes();
                default -> 0;
            };
        } catch (InputMismatchException e) {
            System.out.println("Jus ievadijat nepareizu laiku. Meģiniet velreiz!");
            minutes = chooseMinutes();
        }
        return minutes;
    }

    @Override
    public String toString() {
        return reason.getName() + " " + appointmentDate.format(DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT));
    }
}
