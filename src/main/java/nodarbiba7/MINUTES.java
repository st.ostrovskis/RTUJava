package nodarbiba7;

public enum MINUTES {
    ZERO(0), TEN(10), TWENTY(20), THIRTY(30), FORTY(40), FIFTY(50);

    private final int minutes;

    MINUTES(int minutes){
        this.minutes = minutes;
    }

    public int getMinutes() {
        return minutes;
    }
}
