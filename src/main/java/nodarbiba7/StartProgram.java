package nodarbiba7;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class StartProgram {

    //Saglaba faila Map kolekciju
    public static void whiteToFile(Map<String,Client> clientList) {
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("clientList.bin");
            ObjectOutputStream  objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeInt(clientList.size());
            for (Map.Entry<String, Client> entry : clientList.entrySet()){
                objectOutputStream.writeObject(entry.getValue());
            }
            objectOutputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    //Izveido no faila Map kolekciju
    public static Map<String,Client> readFromFile(){
        Map<String,Client> clientMap = new HashMap<>();
        try{
            FileInputStream fileInputStream = new FileInputStream("clientList.bin");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            int count = objectInputStream.readInt();
            for (int i = 0; i<count; i++){
                Client client = (Client) objectInputStream.readObject();
                clientMap.put(client.getName().toLowerCase() + " " + client.getLastname().toLowerCase(),client);
            }
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return clientMap;
    }

    public static void main(String[] args) {
        Map<String,Client> clientList =  StartProgram.readFromFile();// no faila tiek izveidota Map kolekcija
        Appointment appointment= new Appointment();// izveidojam viziti
        Client.checkClient(appointment,clientList);//parbaudam vai datu baze ir tads klients

        //Parbaudam ka viss strada))
        System.out.println("Klienti kuri saglabati faila");
        StartProgram.whiteToFile(clientList);
        for (Map.Entry<String, Client> entry : clientList.entrySet()){
            System.out.println(entry.getValue().toString());
        }
    }
}
