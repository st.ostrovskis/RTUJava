package nodarbiba7;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

public class Client implements Serializable {
    private final String name;
    private final String lastname;
    private final int phoneNumber;
    private ArrayList<Appointment> appointments = new ArrayList<>();


    Client(String name, String lastname, int phoneNumber) {
        this.name = changeCase(name);
        this.lastname = changeCase(lastname);
        this.phoneNumber = phoneNumber;
    }

    public void setAppointment(Appointment appointment) {
        appointments.add(appointment);
    }

    public String getName() {
        return name;
    }

    public String getLastname() {
        return lastname;
    }


    //Parveido String lai pirmais burts butu liels un pareji mazie
    private String changeCase(String name) {
        StringBuilder word = new StringBuilder();
        for (int i = 0; i < name.length(); i++) {
            if (i == 0) {
                word.append(name.toUpperCase().charAt(i));
            } else {
                word.append(name.toLowerCase().charAt(i));
            }
        }
        return String.valueOf(word);
    }

    //Parbauda vai klients ir klientu saraksta, ja nav tad izveido jauno klientu un pievieno
   public static void checkClient(Appointment appointment, Map<String, Client> clientList) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ievadiet savu vardu un uzvardu");
        try {
            String name = scanner.next().toLowerCase();
            if (Client.haveDigit(name)) {
                System.out.println("Varda un uzvarda nevar but cipari");
                checkClient(appointment, clientList);
            }
            String lastname = scanner.next().toLowerCase();
            if (Client.haveDigit(lastname)) {
                checkClient(appointment, clientList);
            }
            if (clientList.containsKey(name + " " + lastname)) {
                clientList.get(name + " " + lastname).setAppointment(appointment);
            } else {
                Client client = new Client(name, lastname, Client.createPhoneNumber());
                client.setAppointment(appointment);
                clientList.put(name + " " + lastname, client);
            }
            System.out.println(clientList.get(name + " " + lastname).toString());
        } catch (InputMismatchException e) {
            System.out.println("Vards, Uzvards nevar saturet ciparus. Meģiniet velreiz");
        }
    }

    //parbauda vai String satur ciparus
    public static boolean haveDigit(String word) {
        boolean result = false;
        for (char letter : word.toCharArray()) {
            result = Character.isDigit(letter);
        }
        return result;
    }

    //Izveido telefona numuru
    public static int createPhoneNumber() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ievadiet telefona numuru:");
        int phoneNumber;
        int digitCount = 0;
        try {
            phoneNumber = scanner.nextInt();
            int checkPhoneNumber = phoneNumber;
            while (checkPhoneNumber != 0) {
                checkPhoneNumber /= 10;
                digitCount++;
            }
            //Ja ciparu skaits nav vienads ar 8, piedava ievadit no jauna
            if (digitCount != 8){
                System.out.println("Telefona numura jabut 8 cipariem");
                phoneNumber = createPhoneNumber();
            }

        } catch (InputMismatchException e) {
            System.out.println("Telefona numurs nevar saturet burtus. meģiniet velreiz");
            phoneNumber = createPhoneNumber();
        }
        return phoneNumber;
    }

    @Override
    public String toString() {
        return " Vards,uzvards: " + name + " " + lastname + ", telefona numurs: " + phoneNumber + " " + appointments;
    }
}
