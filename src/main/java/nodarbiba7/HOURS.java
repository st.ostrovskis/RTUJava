package nodarbiba7;

public enum HOURS {
    EIGHT(8), NINE(9), TEN(10), ELEVEN(11), TWELVE(12), THIRTEEN(13),FOURTEEN(14), FIFTEEN(15),
    SIXTEEN(16), SEVENTEEN(17), EIGHTEEN(18);

    private final int hours;

    HOURS(int hours){
        this.hours = hours;
    }

    public int getHours() {
        return hours;
    }
}
