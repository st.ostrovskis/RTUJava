package nodarbiba2.prakstiskais5;

import java.util.Scanner;

public class Praktiskais {
    public static void main(String[] args) {
        int a, b, c;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Ievadiet 1.skaitli: ");
        a = scanner.nextInt();

        System.out.println("Ievadiet 2.skaitli: ");
        b = scanner.nextInt();

        System.out.println("Ievadiet 3.skaitli: ");
        c = scanner.nextInt();


        if (a < b) {

            if (a < c) {
                System.out.println("1.skaitlis " + a + " ir vismazākais.");
            } else if (c < a) {
                System.out.println("3.skaitlis " + c + " ir vismazākais.");
            } else {
                System.out.println("1.skaitlis " + a + " un 3.skaitlis " + c + " ir vienadi un vismazāki.");
            }

        } else if (b < a) {

            if (b < c) {
                System.out.println("2.skaitlis " + b + " ir vismazākais.");
            } else if (c < b) {
                System.out.println("3.skaitlis " + c + " ir vismazākais.");
            } else {
                System.out.println("2.skaitlis " + b + " un 3.skaitlis " + c + " ir vienadi un vismazāki.");
            }

        } else if (a == b) {

            if (a < c) {
                System.out.println("1.skaitlis " + a + " un 2.skaitlis " + b + " ir vienadi un vismazāki.");
            } else if (a > c) {
                System.out.println("3.skaitlis " + c + " ir vismazākais.");
            } else {
                System.out.println("Visi ievaditi skaitļi ir vienadi ");
            }
        }
        scanner.close();
    }
}
