package nodarbiba2.prakstiskais4;

public class Uzdevums3 {
    public static void main(String[] args) {
        final byte N = 10;
        int i = 1;
        while (i < N) {
            System.out.print(i * 2 + ",");
            i++;

        }
        System.out.println();
        System.out.println("i vertība = " + i);
    }
}
