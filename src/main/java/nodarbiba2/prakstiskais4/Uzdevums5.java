package nodarbiba2.prakstiskais4;

import java.util.Scanner;

public class Uzdevums5 {
    public static void main(String[] args) {
        short x;
        System.out.println("Input x");

        Scanner scanner = new Scanner(System.in);
        x = scanner.nextShort();
        scanner.close();

        if (x++ > 0 && x <= 5) {
            System.out.println("x value is valid");
        } else {
            System.out.println("x value is not valid");
        }
    }
}
