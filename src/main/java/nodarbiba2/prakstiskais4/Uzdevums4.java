package nodarbiba2.prakstiskais4;

public class Uzdevums4 {
    public static void main(String[] args) {
        boolean a = true, b = false, c = true, d = false;
        d = a && b;
        System.out.println(d + "");
        d = (b || c) && (a ^ c);
        System.out.println(d + "");
        d = a || b || c;
        System.out.println(d + "");
        d = !a || (c && d) ^ !(c || d);
        System.out.println(d);
    }
}
