package nodarbiba2.prakstiskais4;

public class Uzdevums2 {
    public static void main(String[] args) {
        int x = 34;
        int y = ++x;
        System.out.println("x = " + x + " y = " + y);

        x += 2;
        y = x++;
        System.out.println("x = " + x + " y = " + y);

    }
}
