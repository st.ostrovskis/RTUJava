package nodarbiba4;

public class Praktiskais8 {
    public static void main(String[] args) {
        int[][] numbers = new int[10][10];
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                numbers[i][j] = (i + 1) * (j + 1);
            }
        }
        for (int[] number : numbers) {
            for (int j : number) {
                if (j < 10) {
                    System.out.print("  " + j + " ");
                } else {
                    System.out.print(j + "  ");
                }
            }
            System.out.println(" ");
        }
    }
}
