package nodarbiba4;

import java.sql.SQLOutput;
import java.util.Arrays;

public class Praktiskais9 {

    public static void patskanuSkaits(String... words) {
        char[] patskani = {'a', 'o', 'u', 'e', 'i'};

        //panemam 1 vardu no masiva
        for (int i = 0; i < words.length; i++) {
            int counter = 0; //patskanu skaits

            // cikls burtu parbaudei
            for (int j = 0; j < words[i].length(); j++) {

                // cikls burtu salidzinasanai
                for (int x = 0; x < patskani.length; x++) {
                    if (words[i].charAt(j) == patskani[x]) {
                        counter++;
                    }
                }
            }
            System.out.println("\"" + words[i] + "\"" + " varda patskanu skaits: " + counter);
        }
    }

    public static void main(String[] args) {
        patskanuSkaits("hello", "world", "hh", "aloha");
    }
}
