package nodarbiba6;

public enum SIZE {
    SMALL(20, 1.0), NORMAL(32, 1.3), LARGE(45, 1.7);

    private final int diameter;
    private final double priceCoefficient;

    SIZE(int diameter, double priceCoefficient) {
        this.diameter = diameter;
        this.priceCoefficient = priceCoefficient;
    }

    public int getDiameter() {
        return diameter;
    }

    public double getPriceCoefficient() {
        return priceCoefficient;
    }
}
