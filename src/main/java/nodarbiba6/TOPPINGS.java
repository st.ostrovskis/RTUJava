package nodarbiba6;

public enum TOPPINGS {
    EXTRACHEESE("papildus siers", 3.89), ONIONS("Sipoli", 0.49),
    BACON("Bekons", 4.19), MUSHROOMS("Senes", 2.19),
    GARLIC("Kiploki", 0.0), JALAPENO("Jalapeno pipari", 2.19);

    private final String name;
    private final double  price;

    TOPPINGS(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

}
