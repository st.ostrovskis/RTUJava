package nodarbiba6;

public class Pizza {

    private final NAME name;
    private final SIZE size;
    private final TOPPINGS[] toppings;
    private double totalPrice;


    Pizza(NAME name, SIZE size) {
        this(name, size, null);
        makePrice();
    }

    Pizza(NAME name, SIZE size, TOPPINGS[] toppings) {
        this.name = name;
        this.size = size;
        this.toppings = toppings;
        makePrice();
    }

    //Saliek kopa un atgriež visu papildpiedevu nosaukumus
    private StringBuilder nameOfToppings(TOPPINGS[] toppings) {
        StringBuilder toppingName = new StringBuilder("papildpiedevas: ");

        //Ja papildpiedevu nav, pieliek klat vardu "nav" un pabeidz metodi.
        if (toppings == null) {
            return toppingName.append("nav, ");
        }
        for (TOPPINGS topping : toppings) {
            if (topping == null) {
                break;
            }

            toppingName.append("\"").append(topping.getName()).append("\", ");
        }
        return toppingName;
    }

    //Atgriež papildpiedevu kopejo cenu
    private double priceOfToppings(TOPPINGS[] toppings) {
        //Ja papildpiedevu nav, atgriež nuli
        if (toppings == null) {
            return 0;
        }
        double price = 0;
        for (TOPPINGS topping : toppings) {
            if (topping == null) {
                break;
            }
            price += topping.getPrice();
        }
        return price;
    }

    // Aprekina kopejo summu par picu
    public double makePrice() {

        //Ja papildpiedevu nav, tad reķina bez papildpiedevam
        if (toppings == null) {
            totalPrice = (double) Math.round(((name.getPrice() * size.getPriceCoefficient())) * 100) / 100;
        } else {
            totalPrice = (double) Math.round(((name.getPrice() * size.getPriceCoefficient()) + priceOfToppings(toppings)) * 100) / 100;
        }
        return totalPrice;
    }

    // Parada visu informaciju par pasutitu picu
    public void showPicaInfo() {
        System.out.println("Picas nosaukums: \"" + name.getName() + "\", picas diametrs: " + size.getDiameter() + " cm, " + nameOfToppings(toppings) + "cena : " + totalPrice + " Eiro.");
    }

    public double getTotalPrice() {
        return totalPrice;
    }
}

