package nodarbiba6;

import java.util.Scanner;
import static nodarbiba6.NAME.*;
import static nodarbiba6.SIZE.*;
import static nodarbiba6.TOPPINGS.*;

public class Pasutijums {
    private Pizza[] pizzas;
    private static int numberOfOrder = 0;
    private double orderPrice;

    Pasutijums() {
        numberOfOrder++;   //Katram pasutijumam ir savs unikals numurs
        makePizzas();

        //Ja masiva ir vismaz viena pica, tad parada pasutijuma informaciju
        if (pizzas[0] != null) {
            showOrder(pizzas);
        }
    }

    //Izveido masuvu lidz 5 picam
    private void makePizzas() {
        System.out.println("===========================================");
        System.out.println("Jus varat vienlaikus pastutit lidz 5 picam!");
        System.out.println("===========================================\n");
        Scanner scanner = new Scanner(System.in);
        pizzas = new Pizza[5];
        int pizzaCounter = 0;

        while (pizzaCounter < 5) {
            NAME name = choosepizza();       //Piedeva izvelietes picu
            SIZE size = chooseSize(name);    //Piedeva izvelietis izmeru
            TOPPINGS[] topping;

            //Ja nav izveleta pica vai  izmers, pabeidz programu
            if (name == null | size == null) {
                //Ja masivs pizzas ir tukš, izvada tekstu
                if (pizzas[0] == null){
                System.out.println("Pasutijums atcelts!");
                System.out.println("Visu labu!");}

                // ja masiva ir vismaz 1 pica, pabeidz pasutijumu
                break;
            }

            System.out.println("Vai jus gribat papildpiedevas? " +
                    "1(Ja) : 2(Ne)");
            int addTopping = scanner.nextInt();

            //Ja izvelas pievienot papildpiedevas, tad tik izveidots masivs
            if (addTopping == 1) {
                System.out.println(" ");
                System.out.println("===============================================");
                System.out.println("Jus varat pievienot lidz 3 papildpiedevam!");
                System.out.println("================================================\n");
                topping = chooseTopping();    //Piedeva izvelietis papildpiedevas

                //Ja negrib pievienot papildpiedevas, tad atgriez neko
            } else {
                topping = null;
            }

            //Tiek izveidota pica
            Pizza pizza = new Pizza(name, size, topping);

            System.out.println("----------------------------------------------------------------------------------------");
            pizza.showPicaInfo();       // Parada picas informaciju
            System.out.println("-----------------------------------------------------------------------------------------\n");
            System.out.println("Pievienot pasutijumam? 1(Ja) : 2(Mainit) : 0(Atcelt pasutijumu)");
            int pizzaToOrder = scanner.nextInt();

            //Ja izvelas pievienot picu pasutijumam, pica tiek pievienota masivam
            if (pizzaToOrder == 1) {
                pizzas[pizzaCounter] = pizza;
                orderPrice += pizza.getTotalPrice();
                System.out.println("Pica pievienota pasutijumam.\n");

                //Ja izvelas mainit picu, atgriezas cikla sakuma
            } else if (pizzaToOrder == 2) {
                continue;

                //Ja izvelas atcelt pasutijumu, pabeidz programmu
            } else {
                System.out.println("Pasutijums atcelts!");
                System.out.println("Visu labu!");
                break;
            }
            pizzaCounter++;
            System.out.println("Gribat apskatit savu pasutijumu? 1(Ja) : 2(Ne)");
            int checkOrder = scanner.nextInt();

            //Ja izvelas apskatit pasutijumu, parada informaciju par visam picam masiva
            if (checkOrder == 1) {
                showOrder(pizzas);
            }

            System.out.println(" ");
            System.out.println("Gribat pasutit vel vienu picu? 1(Ja) : 2(Pabeigt pasutijumu)");
            int morePizzas = scanner.nextInt();

            //Ja izvelas pabeigt pasutijumu, tad pabeidz ciklu
            if (morePizzas == 2) {
                break;
            }
        }

        //Ja ir vismaz 3 picas, ir piemerota atlaide 20%
        if (pizzaCounter >= 3) {
            orderPrice = (double) Math.round(orderPrice*0.8 *100)/100;
        }
    }

    //Ļauj izveleties picu
    private NAME choosepizza() {
        NAME pizza;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ievadiet picas numuru:\n" +
                "1(" + MARGARITA.getName() + " " + SMALL.getDiameter() + "cm - " + MARGARITA.getPrice() + " Eiro) : " +
                "2(" + CHEESE.getName() + " " + SMALL.getDiameter() + "cm - " + CHEESE.getPrice() + " Eiro) : " +
                "3(" + SICILIA.getName() + " " + SMALL.getDiameter() + "cm - " + SICILIA.getPrice() + " Eiro) : " +
                "0(Neko nepasutit)");
        int choice = scanner.nextInt();
        pizza = switch (choice) {
            case 1 -> MARGARITA;
            case 2 -> CHEESE;
            case 3 -> SICILIA;
            default -> null;
        };
        return pizza;
    }

    //Ļauj izvelieties  picas izmeru
    private SIZE chooseSize(NAME name) {

        //Ja pica nav izveleta, atgriez nuli
        if (name == null) {
            return null;

        //Ja pica ir izveleta, piedava izveleties izmeru
        } else {
            SIZE size;
            Scanner scanner = new Scanner(System.in);
            System.out.println("Ievadiet picas izmeru\n" +
                    "1(" + SMALL.getDiameter() + "cm - " + (double) Math.round(name.getPrice() * SMALL.getPriceCoefficient() * 100) / 100 + " Eiro) : " +
                    "2(" + NORMAL.getDiameter() + "cm - " + (double) Math.round(name.getPrice() * NORMAL.getPriceCoefficient() * 100) / 100 + " Eiro) : " +
                    "3(" + LARGE.getDiameter() + "cm - " + (double) Math.round(name.getPrice() * LARGE.getPriceCoefficient() * 100) / 100 + " Eiro) : " +
                    "0(Neko nepasutit)");
            int choice = scanner.nextInt();
            size = switch (choice) {
                case 1 -> SMALL;
                case 2 -> NORMAL;
                case 3 -> LARGE;
                default -> null;
            };
            return size;
        }
    }

    ///Pievieno lidz 3 papildpiedevam
    private TOPPINGS[] chooseTopping() {
        TOPPINGS[] toppings = new TOPPINGS[3];
        Scanner scanner = new Scanner(System.in);

        TOPPINGS:
        for (int i = 0; i < toppings.length; i++) {

            //Ja masivs nav tuks, parada kas ir pievienots
            if (i != 0) {
                StringBuilder namesOfToppings = new StringBuilder("Jusu izveletas papildpiedevas :");
                System.out.println(" ");
                System.out.println("-----------------------------------------------------------------------------------");
                for (TOPPINGS topping : toppings) {
                    if (topping == null) {
                        break;
                    }
                    namesOfToppings.append("\"").append(topping.getName()).append("\" ").append(topping.getPrice()).append(" Eiro, ");
                }
                System.out.println(namesOfToppings);
                System.out.println(" ");
                System.out.println("------------------------------------------------------------------------------------\n");
            }

            System.out.println("Ievadiet papildpiedievas numuru:\n" +
                    "1(" + EXTRACHEESE.getName() + "- " + EXTRACHEESE.getPrice() + " Eiro) : " +
                    "2(" + ONIONS.getName() + " - " + ONIONS.getPrice() + " Eiro) " + ": " +
                    "3(" + BACON.getName() + " - " + BACON.getPrice() + " Eiro) " + ":" +
                    "4(" + MUSHROOMS.getName() + " - " + MUSHROOMS.getPrice() + " Eiro) " + ":" +
                    "5(" + GARLIC.getName() + " - " + GARLIC.getPrice() + " Eiro) " + ":" +
                    "6(" + JALAPENO.getName() + " - " + JALAPENO.getPrice() + " Eiro) " + ":" +
                    "0(Nevajag papildpiedevas)");

            int choice = scanner.nextInt();
            switch (choice) {
                case 1:
                    toppings[i] = EXTRACHEESE;
                    break;
                case 2:
                    toppings[i] = ONIONS;
                    break;
                case 3:
                    toppings[i] = BACON;
                    break;
                case 4:
                    toppings[i] = MUSHROOMS;
                    break;
                case 5:
                    toppings[i] = GARLIC;
                    break;
                case 6:
                    toppings[i] = JALAPENO;
                    break;
                default:
                    //Ja negrib pievienot papildpiedevas un nav pievienota neviena papildpiedeva, atgriez tukso masivu
                    if (i == 0) {
                        return  null;
                    }
                    //Ja ir pievienotas papildpiedevas, atgriez masivu ar jau pievienotam papildpiedevam
                    break TOPPINGS;
            }
        }
        return toppings;
    }

    //Parada informaciju par pasutijumu
    private void showOrder(Pizza[] pizzas) {
        System.out.println("---------------------------------------------------------------------------------------------");
        System.out.println("Pasutijuma numurs: " + numberOfOrder);

        //Parbauda visu masivu pec kartas
        for (Pizza pizza : pizzas) {

            //Ja masiva vairak nav objektu, pabeidz ciklu
            if (pizza == null) {
                break;
            }
            pizza.showPicaInfo();
        }
        System.out.println("Kopeja cena: " + (double) Math.round(orderPrice * 100) / 100 + " Eiro.");
        System.out.println("--------------------------------------------------------------------------------------------");
    }


    public static void main(String[] args) {
        Pasutijums pasutijums = new Pasutijums();

    }
}
