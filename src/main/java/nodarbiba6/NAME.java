package nodarbiba6;

public enum NAME {
    MARGARITA("Margarita", 5.99), SICILIA("Sicīliešu", 6.99), CHEESE("Četru sieru", 7.99);

    private final double price;
    private final String name;

    NAME(String name, double price) {
        this.name = name;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }
}

