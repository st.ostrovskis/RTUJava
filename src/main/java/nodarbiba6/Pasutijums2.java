package nodarbiba6;

import static nodarbiba6.NAME.*;
import static nodarbiba6.SIZE.*;
import static nodarbiba6.TOPPINGS.*;

public class Pasutijums2 {
    private static int numberOfOrder;
    private final Pizza[] pizzas = new Pizza[5];
    private double orderPrice;
    private int pizzaCounter;


    Pasutijums2() {
        numberOfOrder++;    //Katram pasutijumam ir savs unikals numurs
    }

    //Izveido picu ar papildpiedevam
    private void makePizza(NAME name, SIZE size, TOPPINGS... toppings) {

        //Ja masiva ja ir 5 picas, tad vairak neizveidos objektu PIZZA
        if (pizzaCounter >= 5) {
            System.out.println("Pasutijuma jau ir 5 picas");
            System.out.println("Jus nevarat pasutit vairak par 5 picam.");
            return;
        }
        Pizza pizza = new Pizza(name, size, toppings);

        //Pievieno izveidotu picu masivam
        pizzas[pizzaCounter] = pizza;
        pizzaCounter++;

        //Ja picu skaits ir 3, samazina par 20% kopejo cenu par 3 picam
        if (pizzaCounter == 3) {
            orderPrice += pizza.makePrice();
            orderPrice = (double) Math.round(orderPrice*80)/100;
        }
        // Ja picu skaits lielaks par 3, pie kopejas cenas pievieno tikai 80% no picas cenas
        else if (pizzaCounter > 3) {
            orderPrice += (double) Math.round(pizza.makePrice()*80)/100;
        }
        //Ja picu skaits mazak par 3, pierekina 100% no picas cenas
        else {
            orderPrice += pizza.makePrice();
        }

    }

    //Izveido picu bez papildpiedevam
    private void makePizza(NAME name, SIZE size) {
        //Ja masiva ja ir 5 picas, tad vairak neizveidos objektu PIZZA
        if (pizzaCounter >= 5) {
            System.out.println("Pasutijuma jau ir 5 picas");
            System.out.println("Jus nevarat pasutit vairak par 5 picam.");
            return;
        }
        Pizza pizza = new Pizza(name, size);

        //Pievieno izveidotu picu masivam
        pizzas[pizzaCounter] = pizza;
        pizzaCounter++;

        //Ja picu skaits ir 3, samazina par 20% kopejo cenu par 3 picam
        if (pizzaCounter == 3) {
            orderPrice += pizza.makePrice();
            orderPrice = (double) Math.round(orderPrice*80)/100;
        }
        // Ja picu skaits lielaks par 3, pie kopejas cenas pievieno tikai 80% no picas cenas
        else if (pizzaCounter > 3) {
            orderPrice += (double) Math.round(pizza.makePrice()*80)/100;
        }
        //Ja picu skaits mazak par 3, pierekina 100% no picas cenas
        else {
            orderPrice += pizza.makePrice();
        }
    }

    //Izvada uz ekrana informaciju par pasutijumu
    public void showOrder() {
        System.out.println("Pasutijuma numurs: " + numberOfOrder);

        //Ja masivs tukš, izvada uz ekrana ka viņš ir tukš
        if (pizzas[0] == null) {
            System.out.println("Tukš grozs.");
            return;
        }

        //Izvada uz ekrana masiva saturu
        for (Pizza pizza : pizzas) {
            if (pizza == null) {
                break;
            }
            pizza.showPicaInfo();
        }

        //Ja masiva vairak par 2 picam, pieraksta ka ir pievienota atlaidi 20%
        if (pizzaCounter >= 3) {
            System.out.println("Jums ir vairak par divam picam pasutijuma, tapec jus sanemat 20% atlaide.");
            System.out.println("Kopeja cena ar atlaidi: " + orderPrice + " Eiro.");
        } else {
            System.out.println("Kopeja cena: " + orderPrice + " Eiro.");
        }
    }


    public static void main(String[] args) {
        Pasutijums2 pasutijums = new Pasutijums2();
        pasutijums.makePizza(MARGARITA, SMALL, BACON, GARLIC);
        pasutijums.makePizza(SICILIA, LARGE);
        pasutijums.makePizza(CHEESE, NORMAL, ONIONS, GARLIC, EXTRACHEESE);
        pasutijums.makePizza(SICILIA, LARGE);
        pasutijums.showOrder();
    }
}
