package nodarbiba3.praktiskais6;

import java.util.Scanner;

public class Uzdevums {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Ievadiet berna vecumu: ");

        int vecums = scanner.nextInt();
        scanner.close();

        if (vecums >= 0 && vecums < 2) {
            System.out.println("Tas ir zīdainis.");
        } else if (vecums >= 2 && vecums < 12) {
            System.out.println("Tas ir mazgadīgais bērns.");
        } else if (vecums >= 12 && vecums < 18) {
            System.out.println("Tas ir pusaudzis.");
        } else if (vecums >= 18 && vecums < 120) {
            System.out.println("Tas ir pieaugušais.");
        } else if (vecums >= 120) {
            System.out.println("Parasti tik ilgi nedzivo.");
        } else {
            System.out.println("Vecums nevar but negativs skaitlis.");
        }
    }
}
