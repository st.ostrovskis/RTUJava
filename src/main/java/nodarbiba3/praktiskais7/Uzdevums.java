package nodarbiba3.praktiskais7;

public class Uzdevums {
    public static void main(String[] args) {
        int x = 20;
        while (x > 0) {

            if (x > 10 && x <= 20) {

                if (x == 20) {
                    System.out.println("This is the 1st loop iteration: the value of variable x is 20.");
                } else if (x == 18) {
                    System.out.println("This is the 2nd loop iteration: the value of variable x is 18.");
                } else if (x == 16) {
                    System.out.println("This is the 3rd loop iteration: the value of variable x is 16.");
                } else if (x == 14) {
                    System.out.println("This is the 4th loop iteration: the value of variable x is 14.");
                } else if (x == 12) {
                    System.out.println("This is the 5th loop iteration: the value of variable x is 12.");
                }
                x -= 2;

            } else if (x <= 10) {

                if (x == 10) {
                    System.out.println("This is the 6th loop iteration: the value of variable x is 10.");
                } else if (x == 7) {
                    System.out.println("This is the 7th loop iteration: the value of variable x is 7.");
                } else if (x == 4) {
                    System.out.println("This is the 8th loop iteration: the value of variable x is 4.");
                } else if (x == 1) {
                    System.out.println("This is the 9th loop iteration: the value of variable x is 1.");
                }
                x -= 3;
            }
        }
    }
}

class CitsVariants {
    public static void main(String[] args) {
        int i = 20;
        while (i > 0) {
            switch (i) {
                case 20:
                    System.out.println("This is the 1st loop iteration: the value of variable x is 20.");
                    break;
                case 18:
                    System.out.println("This is the 2nd loop iteration: the value of variable x is 18.");
                    break;
                case 16:
                    System.out.println("This is the 2nd loop iteration: the value of variable x is 16.");
                    break;
                case 14:
                    System.out.println("This is the 2nd loop iteration: the value of variable x is 14.");
                    break;
                case 12:
                    System.out.println("This is the 2nd loop iteration: the value of variable x is 12.");
                    break;
                case 10:
                    System.out.println("This is the 6th loop iteration: the value of variable x is 10.");
                    break;
                case 7:
                    System.out.println("This is the 7th loop iteration: the value of variable x is 7.");
                    break;
                case 4:
                    System.out.println("This is the 7th loop iteration: the value of variable x is 4.");
                    break;
                case 1:
                    System.out.println("This is the 7th loop iteration: the value of variable x is 1.");
                    break;
            }
            if (i > 10) {
                i -= 2;
            } else {
                i -= 3;
            }
        }
    }
}
