package nodarbiba1.prakstiskais2;

import java.util.Scanner;

public class Uzdevums3 {
    public static void main(String[] args) {
        int skaits;
        int cena;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Ievadiet telpu skaitu.");

        skaits = scanner.nextInt();


        System.out.println("Ir vajadzigs tikai projekts? Ja JĀ - ievadiet TRUE, ja NĒ - ievadiet FALSE.");
        boolean tikaiProjekts = scanner.nextBoolean();

        if (tikaiProjekts) {
            cena = skaits * 30;
        } else {
            cena = skaits * 150;
        }
        scanner.close();
        System.out.println("Tas maksa: " + cena + " EURO");
    }
}
