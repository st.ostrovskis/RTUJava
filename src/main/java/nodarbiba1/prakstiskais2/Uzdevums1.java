package nodarbiba1.prakstiskais2;

import java.util.Scanner;

public class Uzdevums1 {
    public static void main(String[] args) {
        boolean oneMoreTime = true;

        Scanner scanner = new Scanner(System.in);
        while (oneMoreTime) {
            System.out.println("Ievadiet 1. skaitli");
            int a = scanner.nextInt();
            System.out.println("Ievadiet 2. skaitli");
            int b = scanner.nextInt();

            int c = a + b;
            int d = a - b;
            int e = a * b;
            float f = (float) a / b;

            System.out.println(a + " + " + b + " = " + c);
            System.out.println(a + " - " + b + " = " + d);
            System.out.println(a + " * " + b + " = " + e);
            System.out.println(a + " / " + b + " = " + f);

            System.out.println("Vai gribat ievadit citus skaitļus? Ja JĀ  - tad ievadit TRUE, ja NĒ - tad ievadiet FALSE.");
            oneMoreTime = scanner.nextBoolean();

        }
        scanner.close();
    }
}
