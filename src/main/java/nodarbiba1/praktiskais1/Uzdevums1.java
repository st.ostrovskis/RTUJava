package nodarbiba1.praktiskais1;

import java.util.Scanner;

public class Uzdevums1 {
    public static void main(String[] args) {
        boolean isIndividual;
        boolean is18AgeOrMore;
        boolean haveLastYearFine;
        boolean lastYearLessThen3Fine;

        Scanner scanner = new Scanner(System.in);
        System.out.println("Is person individual? If YES - write TRUE, if NOT - write FALSE. ");
        isIndividual = scanner.nextBoolean();

        if (isIndividual) {
            System.out.println("Are you 18 age old or older? If YES - write TRUE, if NOT - write FALSE.");
            is18AgeOrMore = scanner.nextBoolean();

            if (is18AgeOrMore) {
                System.out.println("Do you have during last year any fines? If YES - write TRUE, if NOT - write FALSE");
                haveLastYearFine = scanner.nextBoolean();

                if (haveLastYearFine) {
                    System.out.println("Do you have 1 - 3 fines? If YES - write TRUE, if NOT - write FALSE");
                    lastYearLessThen3Fine = scanner.nextBoolean();

                    if (lastYearLessThen3Fine) {
                        System.out.println("Your fine is: 150 EURO");

                    } else {
                        System.out.println("Your fine is: 500 EURO");
                    }
                } else {
                    System.out.println("Your fine is: 50 EURO.");
                }
            } else {
                System.out.println("You will get a warning.");
            }
        } else {
            System.out.println("Write your annual income:");
            System.out.println("Your fine is: " + scanner.nextInt() / 100 + " EURO");
        }
    }
}
