package nodarbiba1.praktiskais1;

import java.util.Scanner;

public class Uzdevums5 {
    public static void main(String[] args) {
        int summa;
        Scanner scanner = new Scanner(System.in);

        System.out.println("Lūdzu, ievadiet apmeklētāju skaitu! ");
        int a = scanner.nextInt();

        System.out.println("Tostarp bērni līdz 18 gadu vecumam: ");
        int b = scanner.nextInt();

        if (b > 0) {
            System.out.println("Cik bērni ir pirmsskolas vecumā?");
            int c = scanner.nextInt();
            int d = a - b;
            int e = b - c;
            summa = d * 5 + e * 2;
        } else {
            summa = a * 5;
        }

        scanner.close();
        System.out.print("Apmeklējums maksās:");
        System.out.print(summa);

    }
}
