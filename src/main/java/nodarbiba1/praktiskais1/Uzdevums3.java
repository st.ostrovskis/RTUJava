package nodarbiba1.praktiskais1;

import java.util.Scanner;

public class Uzdevums3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input a ");
        int a = scanner.nextInt();
        scanner.close();
        if (a > 0 && a < 5) {
            a = a * 2;
        } else {
            a = a + 8;
        }
        System.out.println(a);
    }
}
