package nodarbiba1.praktiskais1;

import java.util.Scanner;

public class Uzdevums6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.println("Input a ");

            int a = scanner.nextInt();
            a = a % 2;
            if (a == 0) {
                System.out.println("Ievadītais skaitlis ir pāra");
            } else {
                System.out.println("Ievadītais skaitlis ir nepāra.");
            }
        }
    }
}
