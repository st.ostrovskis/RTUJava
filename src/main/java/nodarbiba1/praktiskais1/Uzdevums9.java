package nodarbiba1.praktiskais1;

public class Uzdevums9 {
    static int f1 = 1, f2 = 1, sum;

    public static int recurse(int n) {
        sum = f1 + f2;
        f2 = f1;
        f1 = sum;
        if (n == 0) {
            return 0;
        }
        System.out.print(sum + " ");
        return recurse(n - 1);
    }

    public static int recurse2(int f1, int f2, int n) {
        int sum;
        sum = f1 + f2;
        f2 = f1;
        f1 = sum;
        if (n == 0) {
            return 0;
        }
        System.out.print(sum + " ");
        return recurse2(f1, f2, n - 1);
    }

    public static void loopWhile(int n) {
        int f1 = 1, f2 = 1, sum;
        while (n > 0) {
            sum = f1 + f2;
            f2 = f1;
            f1 = sum;
            System.out.print(sum + " ");
            n--;
        }
    }


    public static void main(String[] args) {
        recurse2(1, 1, 8);
        System.out.println(" ");
        loopWhile(8);
        System.out.println(" ");
        recurse(8);


    }


}
