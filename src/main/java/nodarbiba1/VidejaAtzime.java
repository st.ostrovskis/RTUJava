package nodarbiba1;

import java.util.Scanner;

public class VidejaAtzime {
    public static void main(String[] args) {
        int summa = 0, skaits = 0, atzime;
        boolean parbaude = true;
        float rezultats;

        Scanner scanner = new Scanner(System.in);
        while (parbaude) {
            System.out.println("Lūdzu ievadiet studenta atzīmi:");
            atzime = scanner.nextInt();
            summa = summa + atzime;
            skaits = skaits + 1;
            System.out.println("Vai visu studentu atzīmes ir ievadītas? Ja jā, ievadiet 0, ja nē -jebkuru citu ciparu vai skaitli!");
            if (scanner.nextInt() == 0) {
                parbaude = false;
            }
        }
        scanner.close();
        rezultats = (float) summa / skaits;
        System.out.println("Vidējā atzīme par kontroldarbu studentu grupā ir:" + rezultats);
    }
}
